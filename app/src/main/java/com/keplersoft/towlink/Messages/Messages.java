package com.keplersoft.towlink.Messages;

public enum Messages {
    PERMISSION_DENIED,
    RATIONALE,
    REQUEST_SUCCESS
}
