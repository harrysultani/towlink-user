package com.keplersoft.towlink;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.libraries.places.api.Places;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.libraries.places.api.model.Place;
import com.keplersoft.towlink.routeHelper.RoutHelper;


public class SearchPlaceFragmentDropoff extends AppCompatActivity implements PlacesAutoCompleteAdapter.ClickListener{

    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_place_new);

        Places.initialize(this, getResources().getString(R.string.google_maps_key));

        recyclerView = (RecyclerView) findViewById(R.id.places_recycler_view);
        ((EditText) findViewById(R.id.place_search)).addTextChangedListener(filterTextWatcher);
        toolbar = findViewById(R.id.toolbar_search);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mAutoCompleteAdapter = new PlacesAutoCompleteAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAutoCompleteAdapter.setClickListener(this);
        recyclerView.setAdapter(mAutoCompleteAdapter);
        mAutoCompleteAdapter.notifyDataSetChanged();
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            if (!s.toString().equals("")) {
                mAutoCompleteAdapter.getFilter().filter(s.toString());
                if (recyclerView.getVisibility() == View.GONE)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                }
            } else {
                if (recyclerView.getVisibility() == View.VISIBLE)
                {
                    recyclerView.setVisibility(View.GONE);
                }
            }
        }
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {

        }
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }
    };

    @Override
    public void click(Place place) {


        String placeAdd =  place.getAddress();
        double destination_Lat1 = place.getLatLng().latitude;
        double destination_Long2 = place.getLatLng().longitude;

        Intent placeAddIntent = new Intent();
        placeAddIntent.putExtra("placeAdd",placeAdd);
        placeAddIntent.putExtra("destinationLat1",destination_Lat1);
        placeAddIntent.putExtra("destinationLong2",destination_Long2);
        setResult(RESULT_OK,placeAddIntent);
        finish();
      //  Log.d("erfanAddress", place.getAddress()+", "+place.getLatLng().latitude+place.getLatLng().longitude);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

