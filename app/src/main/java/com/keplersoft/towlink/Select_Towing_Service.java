package com.keplersoft.towlink;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import com.google.android.material.navigation.NavigationView;
import com.keplersoft.towlink.Notification.Notication_Activity;

import java.util.Calendar;

public class Select_Towing_Service extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {
    private Button continueTowingService;
    private RadioButton nowBtn,laterBtn,local_towning,interstate_towing;
    private LinearLayout linearLayout, vehicle_towing,machine_towing;
    private TextView datePicker,timePicker;
    private DatePickerDialog date_picker;
    private TimePickerDialog time_picker;
    private ImageView vehicle_image,machinary_image;
    private String vehicleType = null;
    private String stateType = null;
    private String timeType = null;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.towing_services_layout);
        setContentView(R.layout.activity_navigation_drawer);

        toolbar = findViewById(R.id.toolbar_navs);
        toolbar.setNavigationIcon(R.drawable.ic_menu_icon);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        continueTowingService = findViewById(R.id.continueToNext);
        nowBtn = findViewById(R.id.now_pick_up);
        laterBtn = findViewById(R.id.later_pick_up);
        linearLayout = findViewById(R.id.date_and_time_picker);
        vehicle_towing = findViewById(R.id.vehicle_towing);
        vehicle_image = findViewById(R.id.v_car);
        machine_towing = findViewById(R.id.machineary_towing);
        machinary_image = findViewById(R.id.machineary_image);
        local_towning = findViewById(R.id.local_towing);
        interstate_towing = findViewById(R.id.interstate_towing);
        datePicker = findViewById(R.id.date_picker);
        timePicker = findViewById(R.id.time_picker);
        vehicle_towing.setOnClickListener(Select_Towing_Service.this);
        machine_towing.setOnClickListener(Select_Towing_Service.this);
        continueTowingService.setOnClickListener(Select_Towing_Service.this);
        local_towning.setOnClickListener(Select_Towing_Service.this);
        interstate_towing.setOnClickListener(Select_Towing_Service.this);
        nowBtn.setOnClickListener(Select_Towing_Service.this);
        laterBtn.setOnClickListener(Select_Towing_Service.this);
        datePicker.setOnClickListener(Select_Towing_Service.this);
        timePicker.setOnClickListener(Select_Towing_Service.this);
        linearLayout.setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        switch (id){

            case R.id.nav_profile:
                intent = new Intent(Select_Towing_Service.this, User_Profile.class);
                startActivity(intent);
                break;

            case R.id.nav_booking:
                Toast.makeText(getApplicationContext(),"Booking Selected",Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_invite:
                Toast.makeText(getApplicationContext(),"invite Selected",Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_payment:

                break;
            case R.id.nav_about:
                intent = new Intent(Select_Towing_Service.this, AboutUs_Activity.class);
                startActivity(intent);
                break;
            case R.id.nav_noti:
                intent = new Intent(Select_Towing_Service.this, Notication_Activity.class);
                startActivity(intent);
                break;
            case R.id.nav_online:

                break;
            case R.id.nav_logout:

                break;
            default:

                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void onClick(View v){

        int id = v.getId();
        switch (id){
            case R.id.vehicle_towing:
                vehicle_towing.setBackgroundColor(getResources().getColor(R.color.lightblue));
                vehicle_image.setImageResource(R.drawable.ic_select_vehicle_service);
                machine_towing.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                machinary_image.setImageResource(R.drawable.ic_deselect_machinery_service);
                vehicleType = "vehicle";
                break;

            case R.id.machineary_towing:
                machine_towing.setBackgroundColor(getResources().getColor(R.color.lightblue));
                machinary_image.setImageResource(R.drawable.ic_select_machinery_service);
                vehicle_towing.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                vehicle_image.setImageResource(R.drawable.ic_deselect_vehicle_service);
                vehicleType = "machinery";
                break;

            case R.id.local_towing:
                interstate_towing.setChecked(false);
                stateType = "local";
                break;

            case R.id.interstate_towing:
                local_towning.setChecked(false);
                stateType = "interstate";
                break;

            case R.id.continueToNext:
                vehicleTypeShow();
                break;

            case R.id.now_pick_up:
                laterBtn.setChecked(false);
                linearLayout.setVisibility(View.GONE);
                timeType = "now";
                break;
            case R.id.later_pick_up:
                linearLayout.setVisibility(View.VISIBLE);
                nowBtn.setChecked(false);
                timeType = "later";
                break;

            case R.id.date_picker:
                dateToSelect();
                break;

            case R.id.time_picker:
                timeToSelect();
                break;

                default:

                    break;
        }
    }


    public void vehicleTypeShow(){

        if (vehicleType != null){

            if (stateType != null){

                if (timeType != null){

                    Toast.makeText(getApplicationContext(), vehicleType+" "+stateType+" "+timeType,Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Select_Towing_Service.this, TowlinkMap_Activity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Select Pick-up time",Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getApplicationContext(),"Select where you want towing service",Toast.LENGTH_SHORT).show();
            }


        }else {
            Toast.makeText(getApplicationContext(),"Select Towing Service",Toast.LENGTH_SHORT).show();
        }
    }

    //Time to pick
    public void timeToSelect(){

        final Calendar myCalendar = Calendar.getInstance();
        final int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minutes = myCalendar.get(Calendar.MINUTE);

        time_picker = new TimePickerDialog(Select_Towing_Service.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                String am_pm;
                String minutes1 = "";
                String hourOfDay1 = "";
                if (hourOfDay > 12)
                {
                    hourOfDay = hourOfDay - 12;
                    am_pm = "PM";
                } else {
                    am_pm = "AM";
                }

                if (minute < 10)
                    minutes1 = "0" + minute;
                else
                    minutes1 = String.valueOf(minute);

                if (hourOfDay < 10)
                    hourOfDay1 = "0" + hourOfDay;
                else
                    hourOfDay1 = String.valueOf(hourOfDay);

                timePicker.setText(hourOfDay1 + ":" + minutes1 + " "+am_pm);
            }
        },hour,minutes,false);

        time_picker.show();
    }
    //Date to pick
    public void dateToSelect(){

        final Calendar myCalendar = Calendar.getInstance();
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        int month = myCalendar.get(Calendar.MONTH);
        int year = myCalendar.get(Calendar.YEAR);

        date_picker = new DatePickerDialog(Select_Towing_Service.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                datePicker.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
            }
        },year,month,day);
        date_picker.show();
    }

}
