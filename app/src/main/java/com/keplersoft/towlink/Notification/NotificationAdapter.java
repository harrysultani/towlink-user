package com.keplersoft.towlink.Notification;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.keplersoft.towlink.R;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    private String[] data;
    public NotificationAdapter(String[] data){
        this.data = data;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.notification_list_item,parent,false);
        return new NotificationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        String mess = data[position];
        holder.txtMessage.setText(mess);



    }

    @Override
    public int getItemCount() {
        return data.length;
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{

        TextView txtMessage;
      //  TextView txtTime;
        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);
            txtMessage = itemView.findViewById(R.id.txtMessage);
           // txtTime = itemView.findViewById(R.id.min_ago);
        }
    }



}
