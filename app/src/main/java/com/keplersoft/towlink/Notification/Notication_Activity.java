package com.keplersoft.towlink.Notification;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.keplersoft.towlink.MainActivity;
import com.keplersoft.towlink.Mobile_Number_Authentication;
import com.keplersoft.towlink.R;

public class Notication_Activity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        toolbar = findViewById(R.id.toolbars);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        setSupportActionBar(toolbar);

        RecyclerView notiList = findViewById(R.id.notificationList);
        notiList.setLayoutManager(new LinearLayoutManager(this));
        String[] messTxt = {"James Abraham is on his way to pick up from Brisbane city.",
                "James Abraham is on his way to pick up from Brisbane city.",
        "James Abraham is on his way to pick up from Brisbane city.",
                "James Abraham is on his way to pick up from Brisbane city.",
        "James Abraham is on his way to pick up from Brisbane city.",
                "James Abraham is on his way to pick up from Brisbane city.",
                "James Abraham is on his way to pick up from Brisbane city."};

        notiList.setAdapter(new NotificationAdapter(messTxt));





    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
