package com.keplersoft.towlink.Notification;

public class NotificationModel {

    private String message;
    private String timeShow;

    public NotificationModel(String message, String timeShow) {
        this.message = message;
        this.timeShow = timeShow;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeShow() {
        return timeShow;
    }

    public void setTimeShow(String timeShow) {
        this.timeShow = timeShow;
    }
}
