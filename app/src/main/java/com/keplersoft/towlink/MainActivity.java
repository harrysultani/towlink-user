package com.keplersoft.towlink;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import com.keplersoft.towlink.Helpers.PrefManager_ND;


public class MainActivity extends AppCompatActivity {

    private Button mobile_no_authentication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);

        mobile_no_authentication = findViewById(R.id.btn_mobile);


        if (PrefManager_ND.getInstance(this).getUserId() != null){
            Intent intent = new Intent(MainActivity.this, Select_Towing_Service.class);
            startActivity(intent);
        }

        mobile_no_authentication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Mobile_Number_Authentication.class);
                startActivity(intent);


            }
        });

    }


}
