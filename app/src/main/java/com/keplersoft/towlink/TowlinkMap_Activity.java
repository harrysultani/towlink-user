package com.keplersoft.towlink;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.keplersoft.towlink.MapLocation.LocationHelper;
import com.keplersoft.towlink.Models.Route;
import com.keplersoft.towlink.routeHelper.GoogleMapHelper;
import com.keplersoft.towlink.routeHelper.RoutHelper;
import com.keplersoft.towlink.routeHelper.RouteParser;
import org.json.JSONException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import es.dmoral.toasty.Toasty;

public class TowlinkMap_Activity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private List<AutocompletePrediction> predictionList;
    private Location mLastKnowLocation;
    private LocationCallback locationCallback;
    private View mapView;
    private LocationHelper location = null;
    private final float DEFAULT_ZOOM = 15;
    private static final int REQUEST_CODE_PERMISSION=100;
    private static final int PLAY_SERVICES_REQUEST_CODE=2001;
    private Toolbar toolbar;
    SupportMapFragment mapFragment;
    private Button continueForRoute;
    private Dialog dialog;
    private TextView add_drop,add_pickup;
    private int AUTOCOMPLETE_REQUEST_CODE = 1;
    private String TAG = "Tow";
    private String pickUpAdd,distinationAdd;
    private double distinationLat,distinationLong;
    private double pickUpLat,pickUpLong;
    private ImageButton locationBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.towlink__map_activity);
        continueForRoute = findViewById(R.id.continue_to_draw_route);
        add_pickup = findViewById(R.id.address_pickup);
        add_drop = findViewById(R.id.address_drop_off);
        locationBtn = findViewById(R.id.location_show_btn);
        dialog = new Dialog(this);
        toolbar = findViewById(R.id.map_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        //map fragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(TowlinkMap_Activity.this);

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        }

//        if (add_drop.getText().equals("")){
//          //  continueForRoute.setVisibility(View.GONE);
//
//        }else {
//          //  continueForRoute.setVisibility(View.VISIBLE);
//        }

        continueForRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add_drop.getText().equals("")){
                    Toasty.warning(getApplicationContext(),"Destination address is not selected",Toasty.LENGTH_SHORT,true).show();
                }else{
                    //  calling do route method

                    LatLng fromLatLang = new LatLng(pickUpLat,pickUpLong);
                    LatLng toLatLang = new LatLng(distinationLat,distinationLong);
                    doRoute(fromLatLang,toLatLang);
                }
            }
        });

        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });

    }

    /**
     * To draw route on google map
     * **/
    ArrayList<Route> routes = null;
    private void doRoute(LatLng src, LatLng dest){
        GoogleMapHelper.getDirections(src, dest, new GoogleMapHelper.NavigationHandler() {
            @Override
            public void onNavigationRecieved(String response) {
                try {
                  //  Log.e("rrroute",response);
                  //  Toast.makeText(TowlinkMap_Activity.this, "drawing route", Toast.LENGTH_SHORT).show();
                    routes = RouteParser.parseRoutesJSON(response);
                    RoutHelper routHelper = new RoutHelper(TowlinkMap_Activity.this,mMap);
                    routHelper.doRout(routes, Color.BLACK);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    //pick up button
    public void currentLocationAutoCompleteActivity(View view){
        Intent intent = new Intent(TowlinkMap_Activity.this, SearchPlaceFragmentPickup.class);
        startActivityForResult(intent,2);
    }

    public void dropoffAutoCompleteActivity(View view){
        Intent intent = new Intent(TowlinkMap_Activity.this, SearchPlaceFragmentDropoff.class);
        startActivityForResult(intent,1);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.uber_style_map));
        // Add a marker in Sydney and move the camera
        final LatLng brisbane = new LatLng(-27.470125, 153.021072);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(brisbane));

        getDeviceLocation();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51){
            if (resultCode == RESULT_OK){
                getDeviceLocation();
            }
        }
        if (requestCode == 1){
            if (resultCode == RESULT_OK){
                distinationAdd = data.getStringExtra("placeAdd");
                distinationLat = data.getDoubleExtra("destinationLat1",0);
                distinationLong = data.getDoubleExtra("destinationLong2",0);
                add_drop.setText(distinationAdd);
            }else if (resultCode == RESULT_CANCELED){
                Toasty.warning(getApplicationContext(),"Destination address is not selected",Toasty.LENGTH_SHORT,true).show();
            }
        }else     if (requestCode == 2){
            if (resultCode == RESULT_OK){
                pickUpAdd = data.getStringExtra("placeAdd");
                pickUpLat = data.getDoubleExtra("pickLat1",0);
                pickUpLong = data.getDoubleExtra("pickLong2",0);
                add_pickup.setText(pickUpAdd);
            }else if (resultCode == RESULT_CANCELED){
               // Toasty.warning(getApplicationContext(),"Pickup address is not selected",Toasty.LENGTH_SHORT,true).show();
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation(){
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()){
                            mLastKnowLocation = task.getResult();
                            if (mLastKnowLocation != null){
                                LatLng userCurrentLocation = new LatLng(mLastKnowLocation.getLatitude(),mLastKnowLocation.getLongitude());
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userCurrentLocation, DEFAULT_ZOOM));
                              //  Log.d(TAG,userCurrentLocation.toString());
                                getCurrentAddress(mLastKnowLocation.getLatitude(),mLastKnowLocation.getLongitude());
                                pickUpLat = mLastKnowLocation.getLatitude();
                                pickUpLong = mLastKnowLocation.getLongitude();
                             //   mMap.addMarker(new MarkerOptions().position(userCurrentLocation).title("Current Location"));
                            }else {
                                location.initializeLocationRequest();
                                locationCallback = new LocationCallback(){
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null){
                                            return;
                                        }
                                        mLastKnowLocation = locationResult.getLastLocation();
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnowLocation.getLatitude(),mLastKnowLocation.getLongitude()), DEFAULT_ZOOM));
                                        location.stopUpdateLocation();
                                    }
                                };
                                location.getLocation();
                            }
                        }else {
                            Toasty.warning(TowlinkMap_Activity.this,R.string.locationunable,Toasty.LENGTH_SHORT,true).show();
                        }
                    }
                });
    }

    //current location address found
    public String getCurrentAddress(double lati,double longti){
        String address = "";
        Geocoder geocoder = new Geocoder(this,Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lati, longti, 1);
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                add_pickup.setText(address);
            } else {
                Toasty.error(this,"Searching Current Address",Toasty.LENGTH_SHORT,true).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }
//
//
//    private void testgetServerData(){
//
//        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
//        StringRequest stringRequest = Requester.createGetStringRequestWithJSONBody(ApiUrls.weburls, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {

//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });
//
//        requestQueue.add(stringRequest);
//
//    }


//    private void testServerData(){
//        JSONObject reqObject = new JSONObject();
////        try {
////            reqObject.put("fname","Hafizullah");
////            reqObject.put("lname","Samim");
////            reqObject.put("email","hafizullah.samim@gmail.com");
////            reqObject.put("phone","+93747911991");
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
//
//        //define a request queue
//        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
//
//        //create a string request
//        StringRequest stringRequest = Requester.createPostStringRequestWithJSONBody("urls", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                // handle server response
//                Log.d("erfan",response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                //handle server error
//            }
//        },reqObject.toString());
//
//        //add your string request to request queue
//
//        requestQueue.add(stringRequest);
//        //done
//
//        /** or shortly you can add all in one statement**/
//        //the following is the short form of above statements
//        VolleySingleton.getInstance().getRequestQueue().add(Requester.createPostStringRequestWithJSONBody("urls", new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//               // Log.d("erfan",response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        },reqObject.toString()));
//    }


}
