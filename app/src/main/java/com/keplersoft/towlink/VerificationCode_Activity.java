package com.keplersoft.towlink;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.keplersoft.towlink.Helpers.KeyboardUtils;
import com.keplersoft.towlink.Helpers.PrefManager_ND;

import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OtpTextView;

public class VerificationCode_Activity extends AppCompatActivity {

    private Button continueButton;
    private Toolbar toolbar;
    private TextView phone_Number;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth phoneAuth;
    private OtpTextView codeText;
    private static String TAG = "erfan";
    private String phoneNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_code_layout);
        phoneAuth = FirebaseAuth.getInstance();

        continueButton = findViewById(R.id.continue_verify);
        phone_Number = findViewById(R.id.my_phone_num);
        codeText = findViewById(R.id.opt_code_set);
        toolbar = findViewById(R.id.toolbars);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        phone_Number.setText(getIntent().getStringExtra("mobileNumber"));
        phoneNum = getIntent().getStringExtra("mobileNumber");
        sendVerificationCode(phoneNum);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String code = codeText.getOTP();
                if (code.isEmpty() || code.length() < 6) {
                    codeText.showError();
                    codeText.requestFocus();
                    return;
                }
                verifyCode(code);
            }
        });

//        continueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(VerificationCode_Activity.this, Verification_Code_Completed.class);
//                startActivity(intent);
//            }
//        });

    }

    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

   // Toasty.success(getApplicationContext(),"Successfull",Toasty.LENGTH_SHORT,true).show();

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        phoneAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                          //  Log.d("erfan", "signInWithCredential:success");
                          //  Toasty.success(getApplicationContext(),"Successfull",Toasty.LENGTH_SHORT,true).show();
                            String user = task.getResult().getUser().getUid();
                            PrefManager_ND.getInstance(VerificationCode_Activity.this).setUserId(user);

                            KeyboardUtils.hideKeyboard(VerificationCode_Activity.this);
                            Intent intent = new Intent(VerificationCode_Activity.this, Verification_Code_Completed.class);
                            startActivity(intent);


                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toasty.error(getApplicationContext(),"Not Successfull",Toasty.LENGTH_SHORT,true).show();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallBack);        // OnVerificationStateChangedCallbacks

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be i
            Log.d(TAG, "onVerificationCompleted:" + credential);

            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Log.d(TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;

            // ...
        }

    };

        @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
