package com.keplersoft.towlink.App;

import android.app.Application;

public class MApplication extends Application {
    private static MApplication app;
    public static MApplication getInstance() {
        return app;
    }



    public MApplication(){
        app = this;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        //SystemClock.sleep(1500);
    }
}
