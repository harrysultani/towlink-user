package com.keplersoft.towlink.Helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager_ND {

    interface PrefKeys{
        String PREF_KEY_IS_WALK_THROUGH_SHOWN = "isWalkThroughShownKey";
        String PREF_KEY_IS_COUNTRIES_SET = "isCountryCodeSet";
        String PREF_KEY_USER_TYPE = "loggedInUserTYPE";
        String PREF_KEY_IS_DRIVER_LOGGED_IN = "isDriverLoggedInv2";
        String PREF_KEY_IS_DEVICE_TOKEN_ADDED = "isDeviceTokenAdded2";
        String PREF_KEY_DRIVER_ID = "loggedInDriverIDv2";
        String PREF_KEY_DRIVER_NAME = "loggedInDriverNamev2";
        String PREF_KEY_DRIVER_FIRST_NAME = "loggedInDriverFirstNamev2";
        String PREF_KEY_DRIVER_LAST_NAME = "loggedInDriverLastNamev2";
        String PREF_KEY_DRIVER_PHONE = "loggedInDriverPhone";
        String PREF_KEY_DRIVER_IMAGE_URL = "loggedInDriverImageUrlv2";
        String PREF_KEY_DRIVER_LAST_LOCATION_LAT = "driverLastKnownLocationLatv2";
        String PREF_KEY_DRIVER_LAST_LOCATION_LNG = "driverLastKnownLocationLngv2";
        String PREF_KEY_DRIVER_BALANCE = "driverBalancev2";
        String PREF_LANGUAGE_KEY = "language_keyv2";
        String PREF_KEY_HAS_RIDE = "hasRidev2";
        String PREF_KEY_HAS_VISIBKE_RIDE = "hasVisibleRidev2";
        String PREF_FCM_TOKEN = "fcm_tokenv2";
        String PREF_DRIVER_ONLINE_STATUS = "canReciverRideOrNotv2";
        String PREF_USER_TOKEN = "userTokenAfterLoginv2";
        String PREF_RID_ID = "userActiveRideIdv2";
        String PREF_LAST_LOCATION_UPDATE_DATE = "prefLastLocationUpdateDatev2";
        String PREF_DEVICE_CHANGED = "device_changedv2";

        String PREF_LAST_LAT = "last_lat_prefv2";
        String PREF_LAST_LNG = "last_long_prefv2";

        String PREF_DEVICE_BLOCKED = "pref.towlink.deviceBlockedv2";
        String PREF_DEVICE_BLOCKED_MSG = "pref.towlink.deviceBlockedMSg2v";
    }

    private static PrefManager_ND instance;
    String PREF_NAME = "TOWLINK_DRIVER_PREFS";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    private PrefManager_ND(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public static PrefManager_ND getInstance(Context context) {
        if (instance == null) {
            instance = new PrefManager_ND(context);
        }
        return instance;
    }

    public SharedPreferences getPref() {
        return pref;
    }


    //
    public boolean isLogIn() {
        return pref.getBoolean(PrefKeys.PREF_KEY_IS_DRIVER_LOGGED_IN, false);
    }

    public void setLogin(boolean isLogin) {
        editor.putBoolean(PrefKeys.PREF_KEY_IS_DRIVER_LOGGED_IN, isLogin);
        editor.commit();
    }
    public boolean isDeviceTokenAdded() {
        return pref.getBoolean(PrefKeys.PREF_KEY_IS_DEVICE_TOKEN_ADDED, false);
    }

    public void setDevuceToken(boolean isAdded) {
        editor.putBoolean(PrefKeys.PREF_KEY_IS_DEVICE_TOKEN_ADDED, isAdded);
        editor.commit();
    }

    public String getUserId() {
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_ID, null);
    }

    public void setUserId(String userId) {
        editor.putString(PrefKeys.PREF_KEY_DRIVER_ID, userId);
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_NAME, null);
    }

    public void setUserName(String userName) {
        editor.putString(PrefKeys.PREF_KEY_DRIVER_NAME, userName);
        editor.commit();
    }

    public String getUserFirstName() {
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_FIRST_NAME, "");
    }

    public String getUserLastName() {
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_LAST_NAME, "");
    }

    public String getUserPhone() {
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_PHONE, null);
    }

    public void setUserPhone(String userPhone) {
        editor.putString(PrefKeys.PREF_KEY_DRIVER_PHONE, userPhone);
        editor.commit();
    }

//    public void setDriverInfo(User_ND user) {
////        editor.putInt(PrefKeys.PREF_KEY_USER_TYPE, driverND.getUserND().);
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_ID, user.getId());
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_FIRST_NAME, user.getFname());
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_LAST_NAME, user.getLname());
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_NAME, user.getDisplayName());
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_PHONE, user.getPhone());
//        editor.putString(PrefKeys.PREF_KEY_DRIVER_IMAGE_URL, user.getProfile());
//        //setUserLoggedIn(true);
//        setUserToken(user.getToken());
//
//        editor.commit();
//
//    }


    public boolean isUserLoggedIn() {
        return pref.getBoolean(PrefKeys.PREF_KEY_IS_DRIVER_LOGGED_IN, false);
    }

    public void setUserLoggedIn(boolean isUserLoggedIn) {
        this.setDeviceChanged(false);
        editor.putBoolean(PrefKeys.PREF_KEY_IS_DRIVER_LOGGED_IN, isUserLoggedIn);
        editor.commit();
    }

    public String getUserProfileUrl() {
        //        if (imgurl != null){
//            imgurl = Urls.URL_UPLOADS + imgurl;
//        }
        return pref.getString(PrefKeys.PREF_KEY_DRIVER_IMAGE_URL, null);
    }

    public void setUserProfileUrl(String imageUrl) {
        editor.putString(PrefKeys.PREF_KEY_DRIVER_IMAGE_URL, imageUrl);
        editor.commit();
    }

    public void logout() {
        editor.remove(PrefKeys.PREF_KEY_DRIVER_ID);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_NAME);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_PHONE);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_FIRST_NAME);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_LAST_NAME);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_IMAGE_URL);
        editor.remove(PrefKeys.PREF_KEY_IS_DRIVER_LOGGED_IN);
//        editor.remove(PrefKeys.PREF_KEY_DRIVER_BALANCE);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_LAST_LOCATION_LNG);
        editor.remove(PrefKeys.PREF_KEY_DRIVER_LAST_LOCATION_LAT);

//        editor.remove(PrefKeys.PREF_KEY_USER_TYPE);
        editor.commit();
    }

    public void setLanguage(String language) {
        editor.putString(PrefKeys.PREF_LANGUAGE_KEY, language);
        editor.commit();
    }

    //    public String getLanguage() {
//        return pref.getString(PrefKeys.PREF_LANGUAGE_KEY, LANG_DEF);
//    }
    public void setDriverOnlineStatus(boolean state) {
        editor.putBoolean(PrefKeys.PREF_DRIVER_ONLINE_STATUS, state);
        editor.commit();
    }

    public boolean getDriverOnlineStatus() {
        return pref.getBoolean(PrefKeys.PREF_DRIVER_ONLINE_STATUS, true);
    }

    public void setUserToken(String token) {
        editor.putString(PrefKeys.PREF_USER_TOKEN, token);
        editor.commit();
    }

    public String getUserToken() {
        return pref.getString(PrefKeys.PREF_USER_TOKEN, "");
    }

    public String getFCMToken() {
        return pref.getString(PrefKeys.PREF_FCM_TOKEN, "");
    }

    public void setFCMToken(String deviceToken) {
        editor.putString(PrefKeys.PREF_FCM_TOKEN, deviceToken);
        editor.commit();
    }

    public boolean hasRide() {
        return pref.getBoolean(PrefKeys.PREF_KEY_HAS_RIDE, false);
    }

    public void setHasRide(boolean hasRide) {
        editor.putBoolean(PrefKeys.PREF_KEY_HAS_RIDE, hasRide);
        editor.commit();
    }
    public boolean hasVisibleRide() {
        return pref.getBoolean(PrefKeys.PREF_KEY_HAS_VISIBKE_RIDE, false);
    }

    public void setHasVisibleRide(boolean hasRide) {
        editor.putBoolean(PrefKeys.PREF_KEY_HAS_VISIBKE_RIDE, hasRide);
        editor.commit();
    }

    public void setActiveRideId(String id){
        editor.putString(PrefKeys.PREF_RID_ID, id);
        editor.commit();
    }
    public String getActiveRideId() {
        return pref.getString(PrefKeys.PREF_RID_ID, "");
    }

    public void setLastLocationUpdateDate(long date){
        editor.putLong(PrefKeys.PREF_LAST_LOCATION_UPDATE_DATE, date);
        editor.commit();
    }
    public long getLastLocationUpdateDate() {
        return pref.getLong(PrefKeys.PREF_LAST_LOCATION_UPDATE_DATE, -1);
    }

    public void setDeviceChanged(boolean isChanged){
        editor.putBoolean(PrefKeys.PREF_DEVICE_CHANGED,isChanged);
        editor.commit();
    }


    public boolean isDeviceChanged(){
        return pref.getBoolean(PrefKeys.PREF_DEVICE_CHANGED,false);
    }

    public void setDeviceBlocked(boolean isBlocked){
        editor.putBoolean(PrefKeys.PREF_DEVICE_BLOCKED,isBlocked);
        editor.commit();
    }


    public boolean isDeviceBlocked(){
        return pref.getBoolean(PrefKeys.PREF_DEVICE_BLOCKED,false);
    }

    public void setDeviceBlockedMsg(String msg){
        editor.putString(PrefKeys.PREF_DEVICE_BLOCKED_MSG,msg);
        editor.commit();
    }


    public String getDeviceBlockedMsg(){
        return pref.getString(PrefKeys.PREF_DEVICE_BLOCKED_MSG,"");
    }

//    public void setLastLatLang(LatLng lastLatLang){
//        String lastLat = String.valueOf(lastLatLang.latitude);
//        String lastLng = String.valueOf(lastLatLang.longitude);
//        editor.putString(PrefKeys.PREF_LAST_LAT,lastLat);
//        editor.putString(PrefKeys.PREF_LAST_LNG,lastLng);
//        editor.commit();
//    }
//
//    public LatLng getLastLatLang() {
//        String latStr = pref.getString(PrefKeys.PREF_LAST_LAT,"0");
//        String lngStr = pref.getString(PrefKeys.PREF_LAST_LNG,"0");
//        return new LatLng(Double.parseDouble(latStr),Double.parseDouble(lngStr));
////        return pref.getLong(PrefKeys.PREF_LAST_LOCATION_UPDATE_DATE, -1);
//    }
//
//    public void setLastLocation(Location lastLocation){
//        setLastLatLang(new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude()));
//        editor.putString("pref_location_provider",lastLocation.getProvider());
//        editor.commit();
//    }
//
//    public Location getLastLocation(){
//        String provider = pref.getString("pref_location_provider","");
//        Location location = new Location(provider);
//        LatLng latLng = getLastLatLang();
//        location.setLatitude(latLng.latitude);
//        location.setLongitude(latLng.longitude);
//        return location;
//    }
}