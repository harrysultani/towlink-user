package com.keplersoft.towlink.routeHelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.keplersoft.towlink.Models.Route;
import java.util.ArrayList;
import java.util.List;
import static com.google.android.gms.maps.model.JointType.ROUND;

public class RoutHelper {
    Context context;
    GoogleMap mMap;
    Marker driverStartMarker,startMarker,endMarker,pickUpMarker,carMarker,startPopupMarker,pickUpPopUpMarker,endPopupMarker,driverLocationMarker;
    Circle distCircle,pickupCircle;
    private ArrayList<Polyline> polylinePaths = new ArrayList<>();
    private ArrayList<Route> routes = new ArrayList<>();
    private Polyline srsDistPolyLine;

    public RoutHelper(Context context){
        this.context = context;
    }

    public RoutHelper(Context context, GoogleMap map){
        this.context = context;
        this.mMap = map;
    }

    public void doRout(ArrayList<Route> routs, int routColor, boolean isToUser, int type, boolean hasActiveChange){
        Log.e("xxxxxxmmmmm","isuser: "+isToUser+", and type:"+hasActiveChange);
        this.routes = routs;
        for (int i = 0; i < routes.size(); i++) {
            Route route = routes.get(i);
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true)
                    .startCap(new SquareCap())
                    .endCap(new SquareCap())
                    .jointType(ROUND)
                    .color(routColor)
                    .width(6);

            polylineOptions.addAll(route.points);
            srsDistPolyLine = mMap.addPolyline(polylineOptions);
            polylinePaths.add(srsDistPolyLine);
        }

        if(!isToUser){
            if(hasActiveChange){
                if(type == 1){
                    MarkerOptions distOption = new MarkerOptions();
                    Bitmap distBit = GoogleMapHelper.getMarkerForSrc(context,false);
//            mMap.addMarker(distOption.position(routes.get(0).points.get(routes.get(0).points.size()-1)).icon(BitmapDescriptorFactory.fromBitmap(GoogleMapHelper_ND.getMarkerHiliter(this))));
                    endMarker = mMap.addMarker(distOption.position(routes.get(0).endLocation).icon(BitmapDescriptorFactory.fromBitmap(distBit)));
                    distCircle = mMap.addCircle(new CircleOptions().center(routes.get(0).endLocation).radius(300).fillColor(0x3B007AFF).strokeColor(Color.alpha(Color.BLUE)));
//                    addPopups(false, isToUser, routes.get(0).duration.text, routes.get(0).endAddress, routes.get(0).endLocation);
                }else{
                    MarkerOptions srcOprion = new MarkerOptions();
                    Bitmap srcBit = GoogleMapHelper.getMarkerForSrc(context,true);
                    startMarker = mMap.addMarker(srcOprion.position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(srcBit)));
                }
            }else{
                MarkerOptions distOption = new MarkerOptions();
                MarkerOptions srcOprion = new MarkerOptions();
                Bitmap srcBit = GoogleMapHelper.getMarkerForSrc(context,true);
                Bitmap distBit = GoogleMapHelper.getMarkerForSrc(context,false);

                startMarker = mMap.addMarker(srcOprion.position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(srcBit)));
                endMarker = mMap.addMarker(distOption.position(routes.get(0).endLocation).icon(BitmapDescriptorFactory.fromBitmap(distBit)));
                distCircle = mMap.addCircle(new CircleOptions().center(routes.get(0).endLocation).radius(300).fillColor(0x3B007AFF).strokeColor(Color.alpha(Color.BLUE)));
//                addPopups(false, isToUser, routes.get(0).duration.text, routes.get(0).endAddress, routes.get(0).endLocation);
            }
        }else{
            MarkerOptions srcOption = new MarkerOptions();
            Bitmap srcBit = GoogleMapHelper.getMarkerForSrc(context,true);
            MarkerOptions distOption = new MarkerOptions();
            Bitmap distBit = GoogleMapHelper.getMarkerForSrc(context,false);
            if(routs.size()>0) {
                pickUpMarker = mMap.addMarker(distOption.position(routes.get(0).endLocation).icon(BitmapDescriptorFactory.fromBitmap(distBit)));
                driverStartMarker = mMap.addMarker(srcOption.position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(srcBit)));
//                addPopups(false, isToUser, routes.get(0).duration.text, routes.get(0).endAddress, routes.get(0).endLocation);
                pickupCircle = mMap.addCircle(new CircleOptions().center(routes.get(0).endLocation).radius(200).fillColor(0x3B007AFF).strokeColor(Color.alpha(Color.BLUE)));
                Bitmap carBitmap = GoogleMapHelper.getCarMarker(context);
                driverLocationMarker = mMap.addMarker(new MarkerOptions().position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(carBitmap)));
                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(new CameraPosition.Builder().target(routs.get(0).endLocation).zoom(16.5f).build()));
            }
        }
    }

    public void doRout(ArrayList<Route> routs,int routColor){
        this.routes = routs;
        for (int i = 0; i < routes.size(); i++) {
            Route route = routes.get(i);
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true)
                    .startCap(new SquareCap())
                    .endCap(new SquareCap())
                    .jointType(ROUND)
                    .color(routColor)
                    .width(5);

            polylineOptions.addAll(route.points);
            srsDistPolyLine = mMap.addPolyline(polylineOptions);
            polylinePaths.add(srsDistPolyLine);

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(route.startLocation).zoom(10.5f).build()));
          //  mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(route.endLocation).zoom(15.0f).build()));


        }

        MarkerOptions srcOption = new MarkerOptions();
        Bitmap srcBit = GoogleMapHelper.getMarkerForSrc(context,true);
        MarkerOptions distOption = new MarkerOptions();
        Bitmap distBit = GoogleMapHelper.getMarkerForSrc(context,false);
        pickUpMarker = mMap.addMarker(distOption.position(routes.get(0).endLocation).icon(BitmapDescriptorFactory.fromBitmap(distBit)));
        driverStartMarker = mMap.addMarker(srcOption.position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(srcBit)));
    }

    TextView tvArrivalValue = null;
    TextView tvDropOffValue = null;
//    private void addPopups(boolean isSrc,boolean isToUser, String time, String place, LatLng latLng){
//        IconGenerator generator = new IconGenerator(context);
//        View iView = ((Activity)context).getLayoutInflater().inflate(R.layout.marker_custom_window_nd,null);
//        iView.findViewById(R.id.srcInfoCard).setVisibility(View.VISIBLE);
//
//            if(isSrc){
//
//                iView.findViewById(R.id.srcInfoCard).setVisibility(View.GONE);
//                iView.findViewById(R.id.distInfoCard).setVisibility(View.VISIBLE);
//                TextView etDuration = iView.findViewById(R.id.etDuration);
//                etDuration.setText(time);
//                TextView etSrc = iView.findViewById(R.id.etSrcAddress);
//                etSrc.setText(place);
//                generator.setContentView(iView);
//                generator.setContentPadding(0,0,0,70);
//                generator.setBackground(context.getResources().getDrawable(R.drawable.shape_marker_nd));
//                Bitmap icon = generator.makeIcon();
//                MarkerOptions tp = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(icon));
//
//                if(isToUser){
//                    pickUpPopUpMarker = mMap.addMarker(tp);
//                }else{
//                    if(isSrc){
//                        startPopupMarker = mMap.addMarker(tp);
//                    }else{
//                        endPopupMarker = mMap.addMarker(tp);
//                    }
//                }
//
//            }else{
//                iView.findViewById(R.id.srcInfoCard).setVisibility(View.VISIBLE);
//                iView.findViewById(R.id.distInfoCard).setVisibility(View.GONE);
//                TextView etDuration = iView.findViewById(R.id.etDuration);
//                TextView tvSrcAddress = iView.findViewById(R.id.etSrcAddress);
//                if(isToUser){
//                    tvSrcAddress.setText(context.getString(R.string.arival_time));
//                    tvArrivalValue = etDuration;
//                }else{
//                    tvSrcAddress.setText(context.getString(R.string.drop_off));
//                    tvDropOffValue = etDuration;
//                }
//                etDuration.setText(time);
//
//                generator.setContentView(iView);
//                generator.setContentPadding(0,0,0,70);
//                generator.setBackground(context.getResources().getDrawable(R.drawable.shape_marker_nd));
//                Bitmap icon = generator.makeIcon();
//                MarkerOptions tp = new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(icon));
//
//                if(isToUser){
//                    if(pickUpPopUpMarker != null)
//                        pickUpPopUpMarker.remove();
//                    pickUpPopUpMarker = mMap.addMarker(tp);
//                }else{
//                    if(isSrc){
//                        if(startPopupMarker != null)
//                            startPopupMarker.remove();
//                        startPopupMarker = mMap.addMarker(tp);
//                    }else{
//                        if(endPopupMarker != null)
//                            endPopupMarker.remove();
//                        endPopupMarker = mMap.addMarker(tp);
//                    }
//                }
//            }
//    }

    public void updatePopupValues(LatLng latLang, int timeValues, boolean isToUser){

//        String timeStr = "";
//        if(timeValues > 60){
//            int mins = timeValues%60;
//            int hr = timeValues/60;
//            timeStr = hr+" "+context.getString(R.string.hr)+", "+mins+" "+context.getString(R.string.mins);
//        }else {
//            timeStr = timeValues+" "+context.getString(R.string.mins);
//        }

//        addPopups(false,isToUser,timeStr,"",latLang);

//        IconGenerator generator = new IconGenerator(context);
//        View iView = ((Activity)context).getLayoutInflater().inflate(R.layout.marker_custom_window_nd,null);
//        iView.findViewById(R.id.srcInfoCard).setVisibility(View.VISIBLE);
//        iView.findViewById(R.id.srcInfoCard).setVisibility(View.VISIBLE);
//        iView.findViewById(R.id.distInfoCard).setVisibility(View.GONE);
//        TextView etDuration = iView.findViewById(R.id.etDuration);
//        TextView tvSrcAddress = iView.findViewById(R.id.etSrcAddress);
//        if(isToUser){
//            tvSrcAddress.setText(context.getString(R.string.arival_time));
//            tvArrivalValue = etDuration;
//        }else{
//            tvSrcAddress.setText(context.getString(R.string.drop_off));
//            tvDropOffValue = etDuration;
//        }
//        etDuration.setText(timeStr);
//
//        generator.setContentView(iView);
//        generator.setContentPadding(0,0,0,70);
//        generator.setBackground(context.getResources().getDrawable(R.drawable.shape_marker_nd));
//        Bitmap icon = generator.makeIcon();
//        MarkerOptions tp = new MarkerOptions().position(latLang).icon(BitmapDescriptorFactory.fromBitmap(icon));
//
//        if(isToUser){
//            if(pickUpPopUpMarker != null){
//                pickUpPopUpMarker.remove();
//            }
//            pickUpPopUpMarker = mMap.addMarker(tp);
//        }else{
//            if(endPopupMarker != null){
//                endPopupMarker.remove();
//            }
//            endPopupMarker = mMap.addMarker(tp);
//        }
    }


    public static int calculateTime(LatLng latLng1, LatLng latLng2){
        Location location = new Location("");
        Location location2 = new Location("");
        location.setLatitude(latLng1.latitude);
        location.setLongitude(latLng1.longitude);
        location2.setLatitude(latLng2.latitude);
        location2.setLongitude(latLng2.longitude);

        float distance = location.distanceTo(location2);
        float time = ((60*distance)/15000);
        return (int) time;
    }

    public void clearMap(boolean isToUser){
        if(isToUser){
            if(driverLocationMarker != null)
                driverLocationMarker.remove();
            if(pickUpMarker != null) pickUpMarker.remove();
            if(driverStartMarker != null) driverStartMarker.remove();
            if(pickUpPopUpMarker != null) pickUpPopUpMarker.remove();
            if(pickupCircle != null) pickupCircle.remove();
        }else{
            if(startPopupMarker != null) startPopupMarker.remove();
            if(endPopupMarker != null) endPopupMarker.remove();
            if(startMarker != null) startMarker.remove();
            if(endMarker != null) endMarker.remove();
            if(distCircle != null) distCircle.remove();
        }
        if(srsDistPolyLine != null) srsDistPolyLine.remove();
        if(polylinePaths != null) polylinePaths.clear();
        if(routes != null) routes.clear();
    }
    public Marker addCarMarker(){
        Bitmap carBitmap = GoogleMapHelper.getCarMarker(context);
        carMarker = mMap.addMarker(new MarkerOptions().position(routes.get(0).startLocation).icon(BitmapDescriptorFactory.fromBitmap(carBitmap)));
        return carMarker;
    }

    public void moveCarMarker(LatLng latLng, boolean isToUser){
        if(isToUser){
            if(driverLocationMarker != null){
                driverLocationMarker.setPosition(latLng);
            }
        }else{
            if(carMarker != null){
                carMarker.setPosition(latLng);
            }
        }
    }

    //private static double lastDistance = -1;
    private static final int minimumDistanceToChange = 15;
    public double updateRoutes(Context context, ArrayList<Polyline> mPolyLines, LatLng currentLocation, double lastDistance){
        if(mPolyLines.size() > 0){
            List<LatLng> mPoints = mPolyLines.get(0).getPoints();
            if(mPoints != null && mPoints.size() > 0){
                double currentDistance = 0;
                double newDistance = 0.0;
               for(int j = 0 ; j<mPoints.size() ; j++){
//                   newDistance = Helper_ND.calculateDistance(mPoints.get(j),currentLocation)*1000;
                   if(newDistance < currentDistance || currentDistance == 0){
                       currentDistance = newDistance;
                   }
                   if(currentDistance <= minimumDistanceToChange){
                       // remove all the points from this point down to 0
                       //Toast.makeText(context, "removing route from point: "+j, Toast.LENGTH_SHORT).show();
                       for(int i = j; i>=0; i--){
                           mPoints.remove(i);
                           mPolyLines.get(0).setPoints(mPoints);
                       }
                       lastDistance = -1;
                       return lastDistance;
                   }
                   if(lastDistance != -1 && currentDistance > lastDistance){
                       lastDistance = -1;
                       //draw new rout.
                       //Toast.makeText(context, "need for new route", Toast.LENGTH_SHORT).show();
                       return lastDistance;
                   }
               }
               lastDistance = currentDistance;
            }
        }
        return lastDistance;
    }


    public Marker getStartMarker() {
        return startMarker;
    }

    public Marker getEndMarker() {
        return endMarker;
    }
    public Marker getPickUpMarker() {
        return pickUpMarker;
    }
    public Marker getDriverStartMarker() {
        return driverStartMarker;
    }

    public Marker getCarMarker() {
        return carMarker;
    }

    public Circle getDistCircle() {
        return distCircle;
    }
    public Circle getPickupCircle() {
        return pickupCircle;
    }

    public Marker getStartPopupMarker() {
        return startPopupMarker;
    }
    public Marker getPickupPopupMarker() {
        return pickUpPopUpMarker;
    }

    public Marker getEndPopupMarker() {
        return endPopupMarker;
    }

    public Marker getDriverLocationMarker() {
        return driverLocationMarker;
    }
    public ArrayList<Polyline> getMyPolyLine(){
        return polylinePaths;
    }
}
