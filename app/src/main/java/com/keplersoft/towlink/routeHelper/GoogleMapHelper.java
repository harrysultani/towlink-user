package com.keplersoft.towlink.routeHelper;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.cardview.widget.CardView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.keplersoft.towlink.App.MApplication;
import com.keplersoft.towlink.Network.Requester;
import com.keplersoft.towlink.Network.VolleySingleton;
import com.keplersoft.towlink.R;

public class GoogleMapHelper {

    public interface NavigationHandler{
        void onNavigationRecieved(String response);
    }

    public static Bitmap getMarkerForSrc(Context context,boolean isSrc) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_custom_marker_nd, null);
        marker.findViewById(R.id.cardSrcIcons).setVisibility(View.VISIBLE);
        CardView innerCard = marker.findViewById(R.id.innerCard);

        if(isSrc){
            innerCard.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }else{
            innerCard.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);
        return bitmap;
    }

    public static Bitmap getCarMarker(Context context) {
        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_custom_marker_nd, null);
        marker.findViewById(R.id.carIcon).setVisibility(View.VISIBLE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);
        return bitmap;
    }


    public static void getDirections(LatLng src, LatLng dest, final NavigationHandler handler) {
        //TODO: this pic of code that commented will work when we get the routes from api
        /*routHelper = new RoutHelper(this,mMap);
        toUserRouteHelper = new RoutHelper(this,mMap);
        toUserRouteHelper.doRout(newRide.getToUserRouts(),Color.BLUE,true);
        routHelper.doRout(newRide.getToDestinationRouts(),Color.BLACK,false);*/
        //TODO: if we could not get the rout through apis

        String origin = String.format("%s,%s", src.latitude, src.longitude);
        String destination = String.format("%s,%s", dest.latitude, dest.longitude);
        String url = "https://maps.googleapis.com/maps/api/directions/json?"
                + "language=en"
                + "&alternatives=" + "false"
                + "&origin=" + origin
                + "&destination=" + destination
                + "&key=" + MApplication.getInstance().getString(R.string.google_maps_key);
        Log.d("tag", "getDirections: url: " + url);
        StringRequest request = Requester.createGetStringRequestWithJSONBody(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                handler.onNavigationRecieved(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }
        });
        VolleySingleton.getInstance().getRequestQueue().add(request);
    }
}
