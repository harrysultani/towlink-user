package com.keplersoft.towlink;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.keplersoft.towlink.Helpers.KeyboardUtils;
import com.keplersoft.towlink.Network.ConnectionHelper;

import es.dmoral.toasty.Toasty;

public class Mobile_Number_Authentication extends AppCompatActivity {

    private Button continueButton;
    private Toolbar toolbar;
    private EditText edit_phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_authentication);
        continueButton = findViewById(R.id.continue_btn_mobile);
        edit_phone_number = findViewById(R.id.phone_number);
        toolbar = findViewById(R.id.toolbars);
        toolbar.setNavigationIcon(R.drawable.ic_back_icon);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);


        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumber = "+61" + edit_phone_number.getText().toString().trim();

                if (mobileNumber.isEmpty()){
                    edit_phone_number.setError(getString(R.string.enterphone));
                    edit_phone_number.requestFocus();
                    return;
                }else  if (mobileNumber.length() < 9){
                    edit_phone_number.setError(getString(R.string.validnumber));
                    edit_phone_number.requestFocus();
                    return;
                }

                if (new ConnectionHelper(Mobile_Number_Authentication.this).isNetworkAvailable() == true){

                    Intent intentMobile = new Intent(Mobile_Number_Authentication.this, VerificationCode_Activity.class);
                    intentMobile.putExtra("mobileNumber",mobileNumber);
                    startActivity(intentMobile);
                }else {
                    Toasty.error(getApplicationContext(),getString(R.string.nonetwork),Toasty.LENGTH_SHORT,true).show();
                }

                KeyboardUtils.hideKeyboard(Mobile_Number_Authentication.this);

            }
        });

    }





    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        if(FirebaseAuth.getInstance().getCurrentUser() != null){
//            Intent mapIntent = new Intent(this, Mobile_Number_Authentication.class);
//            mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(mapIntent);
//        }else {
//
//        }
//    }


}
