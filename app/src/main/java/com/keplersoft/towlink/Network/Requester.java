package com.keplersoft.towlink.Network;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class Requester {

    public static StringRequest createPostStringRequestWithMapParams(String url, Response.Listener<String> responseListener, Response.ErrorListener errorListener, final Map<String, String> input) {
        StringRequest request = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                if (input != null)
                    return input;
                return new HashMap<>();
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                if (volleyError.networkResponse != null && volleyError.networkResponse.data != null) {
                    volleyError = new VolleyError(new String(volleyError.networkResponse.data));
                }
                return volleyError;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    public static StringRequest createPostStringRequestWithJSONBody(String url, Response.Listener<String> responseListener, Response.ErrorListener errorListener, final String mRequestBody) {

        StringRequest request = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("api_Key", "api key if needed");
                headers.put("language", "local language if needed");
                headers.put("token", "token if needed");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? super.getBody() : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return super.getBody();
                }
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    public static StringRequest createPutStringRequestWithJSONBody(String url, Response.Listener<String> responseListener, Response.ErrorListener errorListener, final String mRequestBody) {
        StringRequest request = new StringRequest(Request.Method.PUT, url, responseListener, errorListener) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("api_Key", "api key if needed");
                headers.put("language", "local language if needed");
                headers.put("token", "token if needed");
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? super.getBody() : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    return super.getBody();
                }
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

    public static StringRequest createGetStringRequestWithJSONBody(String url, Response.Listener<String> responseListener, Response.ErrorListener errorListener) {

        StringRequest request = new StringRequest(Request.Method.GET, url, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("api_Key", "api key if needed");
                headers.put("language", "local language if needed");
                headers.put("token", "token if needed");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(20000, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        return request;
    }

}
